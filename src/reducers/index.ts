import { combineReducers } from "redux";

import { counterReducer } from "./counter";
import { isLoggedInReducer } from "./isLoggedIn";

export const rootReducer = combineReducers({
  counter: counterReducer,
  isLoggedIn: isLoggedInReducer
});
