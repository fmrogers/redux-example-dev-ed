import { ReduxCounterActionTypes, ReduxCounterActions } from "./../types";

export const counterReducer = (state = 0, action: ReduxCounterActions) => {
  switch (action.type) {
    case ReduxCounterActionTypes.INCREMENT:
      return state + action.payload;
    case ReduxCounterActionTypes.DECREMENT:
      return state - action.payload;
    default:
      return state;
  }
};
