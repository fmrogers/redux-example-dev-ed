import { ReduxIsLoggedInActionTypes, ReduxSignInAction } from "../types";

export const isLoggedInReducer = (state = false, action: ReduxSignInAction) => {
  switch (action.type) {
    case ReduxIsLoggedInActionTypes.SIGN_IN:
      return !state;
    default:
      return state;
  }
};
