import {
  ReduxCounterActionTypes,
  ReduxIncrementAction,
  reduxDecrementAction
} from "./../types";

export const increment = (amount: number): ReduxIncrementAction => {
  return {
    type: ReduxCounterActionTypes.INCREMENT,
    payload: amount
  };
};

export const decrement = (amount: number): reduxDecrementAction => {
  return {
    type: ReduxCounterActionTypes.DECREMENT,
    payload: amount
  };
};
