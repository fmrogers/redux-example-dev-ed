// SYSTEM STORE
export interface ReduxSystemStore {
  counter: number;
  isLoggedIn: boolean;
}

// COUNTER Action Types
export enum ReduxCounterActionTypes {
  INCREMENT = "INCREMENT",
  DECREMENT = "DECREMENT"
}

// LOGGED Action Types
export enum ReduxIsLoggedInActionTypes {
  SIGN_IN = "SIGN_IN"
}

// COUNTER Actions
export interface ReduxIncrementAction {
  type: ReduxCounterActionTypes.INCREMENT;
  payload: number;
}
export interface reduxDecrementAction {
  type: ReduxCounterActionTypes.DECREMENT;
  payload: number;
}

export type ReduxCounterActions = ReduxIncrementAction | reduxDecrementAction;

// LOGGED Actions
export interface ReduxSignInAction {
  type: ReduxIsLoggedInActionTypes.SIGN_IN;
}

export type ReduxLoggedActions = ReduxSignInAction;
