import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { ReduxSystemStore } from "./../../types";
import { increment, decrement } from "./../../actions";

export default function App(): JSX.Element {
  const counter = useSelector((state: ReduxSystemStore) => {
    return state.counter;
  });

  const isLoggedIn = useSelector((state: ReduxSystemStore) => {
    return state.isLoggedIn;
  });

  const dispatch = useDispatch();

  return (
    <div>
      <h1>Counter {counter}</h1>
      <button onClick={() => dispatch(increment(5))}>+</button>
      <button onClick={() => dispatch(decrement(5))}>-</button>
      {isLoggedIn ? <h3>Logged In</h3> : ""}
    </div>
  );
}
